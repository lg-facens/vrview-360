const functions = require('firebase-functions');
const express = require('express');
const imageRouter = require('./examples/image/imageRouter');
const videoRouter = require('./examples/video/videoRouter');
const app = express();

app.use('/image', imageRouter);
app.use('/video', videoRouter);

exports.app = functions.https.onRequest(app);