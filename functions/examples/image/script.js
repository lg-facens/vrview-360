let vrView;
let scenes = {
    walrus: {
        image: 'https://raw.githubusercontent.com/googlearchive/vrview/master/examples/hotspots/walrus.jpg',
        preview: 'https://raw.githubusercontent.com/googlearchive/vrview/master/examples/hotspots/walrus-preview.jpg',
        hotspots: {
            whaleLeft: {
                pitch: 0,
                yaw: 20,
                radius: 0.05,
                distance: 1
            }
        }
    },
    whaleLeft: {
        image: 'https://raw.githubusercontent.com/googlearchive/vrview/master/examples/hotspots/whale-left.jpg',
        preview: 'https://raw.githubusercontent.com/googlearchive/vrview/master/examples/hotspots/whale-left-preview.jpg',
        hotspots: {
            walrus: {
                pitch: 0,
                yaw: 30,
                radius: 0.05,
                distance: 1
            }
        }
    }
}

function onVRViewLoad() {
    vrView = new VRView.Player('#vrview', {
        width: '100%',
        height: '480',
        preview: 'https://raw.githubusercontent.com/googlearchive/vrview/master/examples/hotspots/blank.png',
        image: 'https://raw.githubusercontent.com/googlearchive/vrview/master/examples/hotspots/blank.png',
        isStereo: false,
        is_autopan_off: true
    });

    vrView.on('ready', function() {
        loadScene('walrus')
    });

    vrView.on('click', function(event) {
        if(event.id) {
            loadScene(event.id);
        }
    });
}

function loadScene(id) {
    vrView.setContent({
        width: '100%',
        height: 480,
        preview: scenes[id].preview,
        image: scenes[id].image,
        isStereo: false,
        is_autopan_off: true
    });

    var newScene = scenes[id];
    var sceneHotspots = Object.keys(newScene.hotspots);
    for(var i = 0; i < sceneHotspots.length; i++) {
        var hotspotKey = sceneHotspots[i];
        var hotspot = newScene.hotspots[hotspotKey];
        vrView.addHotspot(hotspotKey, {
            pitch: hotspot.pitch,
            yaw: hotspot.yaw,
            radius: hotspot.radius,
            distance: hotspot.distance
        });
    }
}

function requestMotionPermission() {
    if(typeof(DeviceMotionEvent) !== "undefined" && typeof(DeviceMotionEvent.requestPermission) === "function") {
        DeviceMotionEvent.requestPermission().then(response => {
            if(response == "granted") {
                window.addEventListener('devicemotion', (e) => {
                    console.log(e);
                });
            }
        }).catch(console.error);
    } else {
        console.log("DeviceMotionEvent is not defined");
    }
}

window.addEventListener('load', () => {
    onVRViewLoad();
    requestMotionPermission();
});