let vrView;

function onVrViewLoad() {
    vrView = new VRView.Player('#vrview', {
        width: '100%',
        height: '480',
        video: 'https://raw.githubusercontent.com/googlearchive/vrview/master/examples/video/congo_2048.mp4',
        is_stereo: true,
        loop: false,
        muted: true
    });
}

function requestMotionPermission() {
    if(typeof(DeviceMotionEvent) !== "undefined" && typeof(DeviceMotionEvent.requestPermission) === "function") {
        DeviceMotionEvent.requestPermission().then(response => {
            if(response == "granted") {
                window.addEventListener('devicemotion', (e) => {
                    console.log(e);
                });
            }
        }).catch(console.error);
    } else {
        console.log("DeviceMotionEvent is not defined");
    }
}

window.addEventListener('load', () => {
    onVrViewLoad();
    requestMotionPermission();
    if(vrView.isPaused) {
        vrView.play();
    }
});