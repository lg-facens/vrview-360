# VRView 360 :eyeglasses:

## :rocket: Guia de inicio Rápido
### Pré Requisitos
[Node](https://nodejs.org/en/download/)  
[Ngrok](https://ngrok.com/)

### Instalação
Clone este repositório em sua pasta de preferencia executando o comando a seguir em uma interface de linha de comando.

```
git clone https://gitlab.com/lg-facens/vrview-360
```

Depois caminhe até a raiz do diretorio e instale as dependencias do projeto com os seguintes comandos, respectivamente.
```
cd vrview-360
npm install
```

Por fim inicie o servidor local com o comando.
```
node server.js
```

Caso queira testar os sensores de movimento e acelerometro localmente, com o ngrok inicie um tunel local, executando o comando a seguir.
```
ngrok http 8080
```

E então acesse o link gerado em um dispositivo móvel.

## Reconhecimentos
- repositório vrview - (https://github.com/googlearchive/vrview)  
códigos de exemplos, disponibilizado na [documentação do vrview](https://developers.google.com/vr/develop/web/vrview-web).

